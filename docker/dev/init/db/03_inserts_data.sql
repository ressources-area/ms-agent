INSERT INTO ressourcesArea.centre (id, nom, adresse, code_postale, ville) VALUES (1, 'Château-Gontier', '17 rue Pierre et Marie Curie', '53200', 'Château-Gontier sur Mayenne');
INSERT INTO ressourcesArea.centre (id, nom, adresse, code_postale, ville) VALUES (2, 'Laval', '4 rue Bois Huisserie', '53000', 'Laval');
INSERT INTO ressourcesArea.centre (id, nom, adresse, code_postale, ville) VALUES (3, 'Mayenne', '70 rue du Terras', '53100', 'Mayenne');

INSERT INTO ressourcesArea.grade(id, intitule) VALUES (1,'Sapeur 1er Classe');
INSERT INTO ressourcesArea.grade(id, intitule) VALUES (2,'Sapeur 2eme Classe');
INSERT INTO ressourcesArea.grade(id, intitule) VALUES (3,'Caporal');
INSERT INTO ressourcesArea.grade(id, intitule) VALUES (4,'Caporal-Chef(fe)');
INSERT INTO ressourcesArea.grade(id, intitule) VALUES (5,'Sergent');
INSERT INTO ressourcesArea.grade(id, intitule) VALUES (6,'Sergent-Chef(fe)');
INSERT INTO ressourcesArea.grade(id, intitule) VALUES (7,'Adjudant');
INSERT INTO ressourcesArea.grade(id, intitule) VALUES (8,'Adjudant-chef(fe)');
INSERT INTO ressourcesArea.grade(id, intitule) VALUES (9,'Pats');

INSERT INTO ressourcesArea.competence(id, intitule) VALUES (1, 'Suap');
INSERT INTO ressourcesArea.competence(id, intitule) VALUES (2, 'Incendie');
INSERT INTO ressourcesArea.competence(id, intitule) VALUES (3, 'Secours Routiers');

INSERT INTO ressourcesArea.agent(id, prenom, nom, matricule, password, role, mail, grade_id, centre_id) VALUES (1, 'Admin', 'Admin', '1234', '$2a$10$a32qexgK4tce33bNBmOSdeu88Q8BNRRwQCGfx0gjSm.ZQ.6LWgI0u', 'ADMIN', 'brichet.b@hotmail.fr', 1, 1);
INSERT INTO ressourcesArea.agent(id, prenom, nom, matricule, password, role, mail, grade_id, centre_id) VALUES (2, 'Benoît', 'Brichet', '4821', '$2a$10$a32qexgK4tce33bNBmOSdeu88Q8BNRRwQCGfx0gjSm.ZQ.6LWgI0u', 'USER', 'brichet.b@hotmail.fr', 5, 1);
INSERT INTO ressourcesArea.agent(id, prenom, nom, matricule, password, role, mail, grade_id, centre_id) VALUES (3, 'Bruno', 'Test', '1111',  '$2a$10$a32qexgK4tce33bNBmOSdeu88Q8BNRRwQCGfx0gjSm.ZQ.6LWgI0u', 'USER', 'brichet.b@hotmail.fr', 1, 2);
INSERT INTO ressourcesArea.agent(id, prenom, nom, matricule, password, role, mail, grade_id, centre_id) VALUES (4, 'Nicolas', 'Selon', '2222',  '$2a$10$a32qexgK4tce33bNBmOSdeu88Q8BNRRwQCGfx0gjSm.ZQ.6LWgI0u', 'USER', 'brichet.b@hotmail.fr', 8, 3);

INSERT INTO ressourcesArea.competence_agent(id, agent_id, formateur, competence_id) VALUES (1, 2, True, 1);
INSERT INTO ressourcesArea.competence_agent(id, agent_id, formateur, competence_id) VALUES (2, 2, False, 2);
INSERT INTO ressourcesArea.competence_agent(id, agent_id, formateur, competence_id) VALUES (3, 2, False, 3);
INSERT INTO ressourcesArea.competence_agent(id, agent_id, formateur, competence_id) VALUES (4, 3, False, 1);
INSERT INTO ressourcesArea.competence_agent(id, agent_id, formateur, competence_id) VALUES (5, 4, True, 1);
INSERT INTO ressourcesArea.competence_agent(id, agent_id, formateur, competence_id) VALUES (6, 4, True, 2);
INSERT INTO ressourcesArea.competence_agent(id, agent_id, formateur, competence_id) VALUES (7, 4, True, 3);
