CREATE TABLE competence (
                            id INT AUTO_INCREMENT NOT NULL,
                            intitule VARCHAR(155) NOT NULL,
                            PRIMARY KEY (id)
);


CREATE TABLE centre (
                        id INT AUTO_INCREMENT NOT NULL,
                        nom VARCHAR(155) NOT NULL,
                        adresse VARCHAR(255) NOT NULL,
                        code_postale VARCHAR(5) NOT NULL,
                        ville VARCHAR(255) NOT NULL,
                        PRIMARY KEY (id)
);


CREATE TABLE grade (
                       id INT AUTO_INCREMENT NOT NULL,
                       intitule VARCHAR(35) NOT NULL,
                       PRIMARY KEY (id)
);


CREATE TABLE agent (
                       id INT AUTO_INCREMENT NOT NULL,
                       prenom VARCHAR(50) NOT NULL,
                       nom VARCHAR(100) NOT NULL,
                       matricule VARCHAR(5) NOT NULL,
                       password VARCHAR(255) NOT NULL,
                       role VARCHAR(50) NOT NULL,
                       mail VARCHAR(155) NOT NULL,
                       grade_id INT NOT NULL,
                       centre_id INT NOT NULL,
                       PRIMARY KEY (id)
);


CREATE TABLE competence_agent (
                                       id INT AUTO_INCREMENT NOT NULL,
                                       agent_id INT NOT NULL,
                                       formateur BOOLEAN DEFAULT false NOT NULL,
                                       competence_id INT NOT NULL,
                                       PRIMARY KEY (id)
);


ALTER TABLE competence_agent ADD CONSTRAINT competence_competenceagent_fk
    FOREIGN KEY (competence_id)
        REFERENCES competence (id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION;

ALTER TABLE agent ADD CONSTRAINT centre_agent_fk
    FOREIGN KEY (centre_id)
        REFERENCES centre (id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION;

ALTER TABLE agent ADD CONSTRAINT grade_agent_fk
    FOREIGN KEY (grade_id)
        REFERENCES grade (id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION;

ALTER TABLE competence_agent ADD CONSTRAINT agent_competenceagent_fk
    FOREIGN KEY (agent_id)
        REFERENCES agent (id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION;
