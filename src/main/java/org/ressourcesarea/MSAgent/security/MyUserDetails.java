package org.ressourcesarea.MSAgent.security;

import org.ressourcesarea.MSAgent.model.dtos.PrincipaleAgentDto;
import org.ressourcesarea.MSAgent.model.entities.Agent;
import org.ressourcesarea.MSAgent.model.mapper.PrincipaleAgentMapper;
import org.ressourcesarea.MSAgent.repository.AgentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class MyUserDetails implements UserDetailsService {
    @Autowired
    private AgentRepository agentRepository;
    @Autowired
    private PrincipaleAgentMapper principaleAgentMapper;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Agent> optionalAgent = agentRepository.findByMatricule(username);
        if (optionalAgent.isPresent()){
            return principaleAgentMapper.fromEntityToDto(optionalAgent.get());
        } else {
            throw new UsernameNotFoundException("User not found with username or email : " + username);
        }
    }
    @Transactional
    public UserDetails loadUserById(Long id) {
        Optional<Agent> optionalAgent = agentRepository.findById(id);
        if (optionalAgent.isPresent()){
            return principaleAgentMapper.fromEntityToDto(optionalAgent.get());
        } else {
            throw new UsernameNotFoundException("User not found");
        }
    }
}
