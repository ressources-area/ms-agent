package org.ressourcesarea.MSAgent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableFeignClients("org.ressourcesarea.MSAgent")
public class MsAgentApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsAgentApplication.class, args);
		System.out.println("MS AGENT OK");
	}

}
