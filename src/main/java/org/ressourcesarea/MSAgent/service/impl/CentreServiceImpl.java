package org.ressourcesarea.MSAgent.service.impl;

import org.ressourcesarea.MSAgent.model.dtos.CentreDto;
import org.ressourcesarea.MSAgent.model.mapper.CentreMapper;
import org.ressourcesarea.MSAgent.repository.CentreRepository;
import org.ressourcesarea.MSAgent.service.contract.CentreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CentreServiceImpl implements CentreService {

    // ----- Injections de dépendances ----- //

    private final CentreRepository centreRepository;
    private final CentreMapper centreMapper;

    @Autowired
    public CentreServiceImpl(CentreRepository centreRepository,
                             CentreMapper centreMapper){
        this.centreRepository = centreRepository;
        this.centreMapper = centreMapper;
    }

    // ----- Méthodes ----- //

    /**
     * {@inheritDoc}
     * @param nom
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public CentreDto getCentreByNom(final String nom) {
        return centreMapper.centreToCentreDto(centreRepository.getCentreByNom(nom));
    }

    /**
     * {@inheritDoc}
     * @return All centre
     */
    @Override
    @Transactional(readOnly = true)
    public List<CentreDto> getAllCentre() {
        return centreMapper.mapToDto(centreRepository.getAllCentre());
    }
}
