package org.ressourcesarea.MSAgent.service.impl;

import org.ressourcesarea.MSAgent.model.dtos.CompetenceDto;
import org.ressourcesarea.MSAgent.model.mapper.CompetenceMapper;
import org.ressourcesarea.MSAgent.repository.CompetenceRepository;
import org.ressourcesarea.MSAgent.service.contract.CompetenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CompetenceServiceImpl implements CompetenceService {

    // ----- Injections des dépendances ----- //

    private final CompetenceRepository competenceRepository;
    private final CompetenceMapper competenceMapper;

    @Autowired
    public CompetenceServiceImpl(CompetenceRepository competenceRepository,
                                 CompetenceMapper competenceMapper){
        this.competenceRepository = competenceRepository;
        this.competenceMapper = competenceMapper;
    }

    // ----- Méthodes ----- //
    /**
     * {@inheritDoc}
     * @param competence
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public CompetenceDto getCompetenceByCompetence(String competence) {
        return competenceMapper.competenceToCompetenceDto(
                competenceRepository.getCompetenceByCompetence(competence));
    }

    @Override
    @Transactional(readOnly = true)
    public List<CompetenceDto> getAllCompetence() {
        return competenceMapper.mapToDto(
                competenceRepository.getAllCompetence()
        );
    }
}
