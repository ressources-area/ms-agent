package org.ressourcesarea.MSAgent.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.ressourcesarea.MSAgent.Utils.feign.HttpProxy;
import org.ressourcesarea.MSAgent.application.error.AuthorizeException;
import org.ressourcesarea.MSAgent.application.error.EntityException;
import org.ressourcesarea.MSAgent.model.dtos.AgentDto;
import org.ressourcesarea.MSAgent.model.dtos.RegistrerDto;
import org.ressourcesarea.MSAgent.model.entities.Agent;
import org.ressourcesarea.MSAgent.model.mapper.AgentMapper;
import org.ressourcesarea.MSAgent.repository.AgentRepository;
import org.ressourcesarea.MSAgent.service.contract.AgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class AgentServiceImpl implements AgentService {

    // ----- Injections des dépendances ----- //

    private final AgentRepository agentRepository;
    private final AgentMapper agentMapper;
    private final PasswordEncoder passwordEncoder;
    private final ValidationService validation;
    private final HttpProxy httpProxy;

    @Autowired
    public AgentServiceImpl(AgentRepository agentRepository,
                            AgentMapper agentMapper,
                            ValidationService validation,
                            HttpProxy httpProxy,
                            PasswordEncoder passwordEncoder) {
        this.agentRepository = agentRepository;
        this.agentMapper = agentMapper;
        this.passwordEncoder = passwordEncoder;
        this.validation = validation;
        this.httpProxy = httpProxy;
    }

    // ----- Méthodes ----- //

    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<AgentDto> getListAgent() {
        return agentMapper.mapToDto(agentRepository.getListAgent());
    }

    /**
     * {@inheritDoc}
     *
     * @param matricule
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public AgentDto getProfil(final String matricule) {
        return agentMapper.fromEntityToDto(agentRepository.getAgentByMatricule(matricule));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public AgentDto getAgentById(final int idAgent) {
        return agentMapper.fromEntityToDto(agentRepository.getAgentById(idAgent));
    }

    @Override
    @Transactional(readOnly = true)
    public AgentDto getAgentByMatricule(final String matricule) {
        return agentMapper.fromEntityToDto(agentRepository.getAgentByMatricule(matricule));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public List<AgentDto> getAgentByCentre(final String nomCentre) {
        return agentMapper.mapToDto(agentRepository.getAgentByCentre(nomCentre));
    }

    /**
     * {@inheritDoc}
     *
     * @param nomPrenom
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<AgentDto> getAgentListBySearch(String nomPrenom) {
        nomPrenom = nomPrenom.replaceAll("\\s", "");
        nomPrenom = nomPrenom + "%";
        nomPrenom = nomPrenom.toLowerCase();
        nomPrenom = uppedCaseFirst(nomPrenom);
        return agentMapper.mapToDto(agentRepository.getAgentListBySearch(nomPrenom));
    }

    /**
     * {@inheritDoc}
     *
     * @param matriculeList
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<AgentDto> getAgentNotInStage(final List<String> matriculeList) {

        List<AgentDto> agentDtoList = agentMapper.mapToDto(agentRepository.getListAgent());
        List<AgentDto> newListAgent = new ArrayList<>();

        // On parcours la liste des agents
        for (AgentDto agentDto : agentDtoList) {
            if (!agentDto.getGrade().getIntitule().equals("Pats")) {
                if (!checkIfMatriculeExistInStage(agentDto, matriculeList)) {
                    newListAgent.add(agentDto);
                }
            }
        }
        return newListAgent;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void createAgent(RegistrerDto registrerDto) {
        try {
            if ((agentRepository.getAgentByMatricule(registrerDto.getMatricule())) == null) {
                    if (validation.validationCreateUser(registrerDto)) {
                        AgentDto agentDto = new AgentDto();
                        agentDto.setCentre(registrerDto.getCentre());
                        agentDto.setGrade(registrerDto.getGrade());
                        agentDto.setNom(registrerDto.getNom());
                        agentDto.setPrenom(registrerDto.getPrenom());
                        agentDto.setMatricule(registrerDto.getMatricule());
                        agentDto.setRole("USER");
                        Agent agent = agentMapper.fromDtoToEntity(agentDto);
                        String hashPW = passwordEncoder.encode(registrerDto.getPassword());
                        agent.setPassword(hashPW);

                        agentRepository.save(agent);
                    }
            }
            else {
                throw new EntityException("Ce matricule est déjà utilisé");
            }
        }
        catch (EntityException e) {
            log.error("Erreur de création d'un agent");
            throw new EntityException("Erreur lors de la création de l'agent");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void updateAgent(AgentDto agentDto) {
        try {
            if (validation.checkAgentValid(agentDto)) {
                Agent agent = agentRepository.getAgentById(agentDto.getId());
                Agent tempAgent = agentMapper.fromDtoToEntity(agentDto);
                agent.setCentre(tempAgent.getCentre());
                agent.setGrade(tempAgent.getGrade());
                agent.setNom(agentDto.getNom());
                agent.setPrenom(agentDto.getNom());
                agent.setMatricule(agentDto.getMatricule());
                agent.setRole(agentDto.getRole());
                agentRepository.save(agent);
            }
        }
        catch (EntityException e) {
            log.error("Erreur modification agent");
            throw new EntityException("Erreur lors de la modification de l'agent");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public List<AgentDto> getAgentByCompetenceAndFormateur(final String intituleCompetence) {
        return agentMapper.mapToDto(
                agentRepository.getAgentByCompetenceAndFormateur(intituleCompetence));
    }

    /**
     * {@inheritDoc}
     *
     * @param agentDto
     * @param oldPassword
     * @param newPassword
     */
    @Override
    @Transactional
    public void updatePassword(AgentDto agentDto, String oldPassword, String newPassword) {
        Agent agent = agentRepository.getAgentById(agentDto.getId());
        if (passwordEncoder.matches(oldPassword, agent.getPassword())) {
            if (validation.checkPasswordValid(newPassword)) {
                newPassword = passwordEncoder.encode(newPassword);
                agent.setPassword(newPassword);
                agentRepository.save(agent);
            }
        }
        else {
            throw new AuthorizeException("L'ancien mot de passe n'est pas correct");
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param agentDto
     */
    @Override
    @Transactional
    public void changeRole(final AgentDto agentDto) {
        switch (agentDto.getRole()) {
            case "ADMIN":
                agentRepository.updateRole("USER",agentDto.getId());
                break;
            case "USER":
                agentRepository.updateRole("ADMIN",agentDto.getId());
                break;
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param idAgent
     */
    @Override
    @Transactional
    public void deleteAgent(final Integer idAgent) {
        Agent agent = agentRepository.getAgentById(idAgent);
        if (agent != null) {
            agentRepository.delete(agentRepository.getAgentById(idAgent));
        }
        else {
            log.error("Erreur de suppression");
            throw new EntityException("L'agent n'existe pas");
        }
    }

    private String uppedCaseFirst(String value) {
        char[] arr = value.toCharArray();
        arr[0] = Character.toUpperCase(arr[0]);
        return new String(arr);
    }

    private Boolean checkIfMatriculeExistInStage(AgentDto agentDto, List<String> matriculeList) {
        for (String matricule : matriculeList) {
            if (agentDto.getMatricule().equals(matricule)) {
                return true;
            }
        }
        return false;
    }
}

