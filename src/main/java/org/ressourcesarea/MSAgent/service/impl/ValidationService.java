package org.ressourcesarea.MSAgent.service.impl;

import org.ressourcesarea.MSAgent.application.error.EntityException;
import org.ressourcesarea.MSAgent.model.dtos.AgentDto;
import org.ressourcesarea.MSAgent.model.dtos.RegistrerDto;
import org.ressourcesarea.MSAgent.model.mapper.AgentMapper;
import org.ressourcesarea.MSAgent.repository.AgentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class ValidationService {

    private final AgentMapper agentMapper;
    private final AgentRepository agentRepository;

    @Autowired
    public ValidationService(AgentMapper agentMapper,
                             AgentRepository agentRepository){
        this.agentMapper = agentMapper;
        this.agentRepository = agentRepository;
    }


    public Boolean validationCreateUser(RegistrerDto registrerDto){
        Boolean validation = false;

        if (registrerDto.getPassword().equals(registrerDto.getConfirmationPassword())){
            validation = true;
        } else {
            throw new EntityException("Les mots de passe ne sont pas identiques");
        }

        if (registrerDto.getPassword().length() > 6) {
            validation = true;
        } else {
            throw new EntityException("Le mot de passe doit contenir au minimum 6 caractères");
        }

        if(registrerDto.getMatricule().length() == 4) {
            validation = true;
        } else {
            throw new EntityException("Le matricule doit contenir 4 chiffres");
        }

        return validation;
    }

    /* Méthode qui valide la conformité du mot de passe */
    Boolean checkPasswordValid(String password){
        if (password.length() < 6){
            throw new EntityException("Le mot de passe doit contenir au minimum 6 caractère");
        }
        return true;
    }

    /* Méthode qui valide la conformité de la création d'un agent */
    Boolean checkAgentValid(AgentDto agentDto){
        // Nom, Prénom, Grade, Centre, role sont requis
        // Mail est au format mail
        // Matricule est égale à 4 chiffres

        if (agentDto.getNom() == null ||
                agentDto.getPrenom() == null ||
                agentDto.getGrade() == null ||
                agentDto.getCentre() == null ||
                agentDto.getRole() == null){
            throw new EntityException("Veuillez remplir tout les champs obligatoires");
        }
//        String p = ("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$");
//        AgentDto agentDtoExist = agentMapper.fromEntityToDto(agentRepository.getAgentByMail(
//                agentDto.getMail()
//        ));
//
//
//
//        if(agentDtoExist != null){
//            if(!agentDto.getMail().equals(agentDtoExist.getMail())){
//                if (agentDtoExist != null){
//                    throw new EntityException("L'adresse mail est déjà utilisée");
//                }
//            }
//        }
//        if (!isValid(agentDto.getMail())){
//            throw new EntityException("L'adresse mail n'est pas valide");
//        }

        if (!agentDto.getMatricule().matches("[0-9]+") && agentDto.getMatricule().length() != 4){
            throw new EntityException("Le matricule ne contient pas 4 chiffres");
        }
        return true;
    }

    public static boolean isValid(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

}
