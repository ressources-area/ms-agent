package org.ressourcesarea.MSAgent.service.impl;

import org.ressourcesarea.MSAgent.model.dtos.GradeDto;
import org.ressourcesarea.MSAgent.model.mapper.GradeMapper;
import org.ressourcesarea.MSAgent.repository.GradeRepository;
import org.ressourcesarea.MSAgent.service.contract.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GradeServiceImpl implements GradeService {

    // ----- Injections des dépendances ----- //

    private final GradeRepository gradeRepository;
    private final GradeMapper gradeMapper;

    @Autowired
    public GradeServiceImpl(GradeRepository gradeRepository,
                            GradeMapper gradeMapper){
        this.gradeRepository = gradeRepository;
        this.gradeMapper = gradeMapper;
    }


    // ----- Méthodes ----- //

    /**
     * {@inheritDoc}
     * @param intitule
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public GradeDto getGradeByIntitule(final String intitule) {
        return gradeMapper.gradeToGradeDto(gradeRepository.getGradeByIntitule(intitule));
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<GradeDto> getAllGrade() {
        return gradeMapper.mapGradeDtos(gradeRepository.getAllGrade());
    }
}
