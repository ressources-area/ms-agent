package org.ressourcesarea.MSAgent.service.impl;

import org.ressourcesarea.MSAgent.Utils.security.CookieUtil;
import org.ressourcesarea.MSAgent.model.dtos.*;
import org.ressourcesarea.MSAgent.model.entities.Agent;
import org.ressourcesarea.MSAgent.model.mapper.AgentMapper;
import org.ressourcesarea.MSAgent.repository.AgentRepository;
import org.ressourcesarea.MSAgent.security.JwtTokenProvider;
import org.ressourcesarea.MSAgent.service.contract.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Service
public class UserServiceImpl implements UserService {

    private final AgentRepository agentRepository;
    private final JwtTokenProvider jwtTokenProvider;
    private final CookieUtil cookieUtil;
    private final AgentMapper agentMapper;

    @Autowired
    public UserServiceImpl(AgentRepository agentRepository,
                           JwtTokenProvider jwtTokenProvider,
                           CookieUtil cookieUtil,
                           AgentMapper agentMapper){
        this.agentRepository = agentRepository;
        this.jwtTokenProvider = jwtTokenProvider;
        this.cookieUtil = cookieUtil;
        this.agentMapper = agentMapper;
    }

    @Override
    public ResponseEntity<LoginResponse> login(final LoginRequest loginRequest, final String accessToken, final String refreshToken) {
        String matricule = loginRequest.getMatricule();
        Agent agent = agentRepository.findByMatricule(matricule).orElseThrow(
                ()-> new IllegalArgumentException("Agent non trouvé avec ce matricule"+matricule));

        PrincipaleAgentDto principaleAgentDto = new PrincipaleAgentDto();
        principaleAgentDto.setMatricule(agent.getMatricule());
        principaleAgentDto.setRole(agent.getRole());

        Boolean accessTokenValid = jwtTokenProvider.validateToken(accessToken);
        Boolean refreshTokenValid = jwtTokenProvider.validateToken(refreshToken);

        HttpHeaders responseHeaders = new HttpHeaders();
        Token newAccessToken = null;
        Token newRefreshToken;
        if (!accessTokenValid && !refreshTokenValid){
            newAccessToken = jwtTokenProvider.generateToken(principaleAgentDto);
            newRefreshToken = jwtTokenProvider.generateRefreshToken(principaleAgentDto);
            addAccessTokenCookie(responseHeaders, newAccessToken);
            addRefreshTokenCookie(responseHeaders, newRefreshToken);
        }

        if (!accessTokenValid && refreshTokenValid){
            newAccessToken = jwtTokenProvider.generateToken(principaleAgentDto);
            addAccessTokenCookie(responseHeaders,newAccessToken);
        }

        if (accessTokenValid && refreshTokenValid){
            newAccessToken = jwtTokenProvider.generateToken(principaleAgentDto);
            newRefreshToken = jwtTokenProvider.generateRefreshToken(principaleAgentDto);
            addAccessTokenCookie(responseHeaders, newAccessToken);
            addRefreshTokenCookie(responseHeaders, newRefreshToken);
        }


        LoginResponse loginResponse = new LoginResponse(LoginResponse.SuccessFailure.SUCCESS,
                "Authentification réussie et cookie crée avec succes", newAccessToken.getTokenValue());

        return ResponseEntity.ok().headers(responseHeaders).body(loginResponse);
    }

    @Override
    public ResponseEntity<LoginResponse> refresh(final String accessToken, final String refreshToken) {
        Boolean refreshTokenValid = jwtTokenProvider.validateToken(refreshToken);
        if (!refreshTokenValid){
            LoginResponse loginResponse = new LoginResponse(LoginResponse.SuccessFailure.FAILURE,
                    "Refresh Token invalid", accessToken);
            return ResponseEntity.ok().body(loginResponse);
        }

        String matricule = jwtTokenProvider.getUsername(refreshToken);

        Agent agent = agentRepository.findAgentWithMatricule(matricule).orElseThrow(
                ()-> new IllegalArgumentException("Agent non trouvé avec le matricule "+matricule)
        );

        PrincipaleAgentDto principaleAgentDto = new PrincipaleAgentDto();
        principaleAgentDto.setMatricule(agent.getMatricule());
        principaleAgentDto.setRole(agent.getRole());

        Token newAccessToken = jwtTokenProvider.generateToken(principaleAgentDto);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(HttpHeaders.SET_COOKIE, cookieUtil.createAccessTokenCookie(
                newAccessToken.getTokenValue(), newAccessToken.getDuration()).toString());

        LoginResponse loginResponse = new LoginResponse(LoginResponse.SuccessFailure.SUCCESS,
                "Authentification réussie, token crée avec success", newAccessToken.getTokenValue());

        return ResponseEntity.ok().headers(responseHeaders).body(loginResponse);
    }

    @Override
    public PrincipaleAgentDto getUserProfile() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Agent agent = agentRepository.findAgentWithMatricule(authentication.getName()).orElseThrow(
                ()-> new IllegalArgumentException("Agent non trouvé avec le matricule "+authentication.getName())
        );

        AgentDto agentDto = agentMapper.fromEntityToDto(agent);
        PrincipaleAgentDto principaleAgentDto = new PrincipaleAgentDto();
        principaleAgentDto.setMatricule(agentDto.getMatricule());
        principaleAgentDto.setRole(agentDto.getRole());

        return principaleAgentDto;
    }

    @Override
    public String logout(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) {

        HttpSession session = httpServletRequest.getSession(false);
        SecurityContextHolder.clearContext();
        session = httpServletRequest.getSession(false);

        if (session != null){
            session.invalidate();
        }

        if (httpServletRequest.getCookies() != null){
            System.out.println("Cookie trouvé");
            for (Cookie cookie : httpServletRequest.getCookies()) {
                cookie.setMaxAge(0);
                cookie.setValue("");
                cookie.setHttpOnly(true);
                cookie.setPath("/");
                httpServletResponse.addCookie(cookie);
            }
        }

        return "Déconnexion réussie";
    }


    private void addAccessTokenCookie(HttpHeaders httpHeaders, Token token) {
        httpHeaders.add(HttpHeaders.SET_COOKIE, cookieUtil.createAccessTokenCookie(token.getTokenValue(), token.getDuration()).toString());
    }

    private void addRefreshTokenCookie(HttpHeaders httpHeaders, Token token) {
        httpHeaders.add(HttpHeaders.SET_COOKIE, cookieUtil.createRefreshTokenCookie(token.getTokenValue(), token.getDuration()).toString());
    }

}
