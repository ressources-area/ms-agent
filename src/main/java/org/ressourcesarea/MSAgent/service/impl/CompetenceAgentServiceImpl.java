package org.ressourcesarea.MSAgent.service.impl;

import org.ressourcesarea.MSAgent.application.error.EntityException;
import org.ressourcesarea.MSAgent.model.dtos.AgentDto;
import org.ressourcesarea.MSAgent.model.dtos.CompetenceAgentDto;
import org.ressourcesarea.MSAgent.model.mapper.AgentMapper;
import org.ressourcesarea.MSAgent.model.mapper.CompetenceAgentMapper;
import org.ressourcesarea.MSAgent.repository.AgentRepository;
import org.ressourcesarea.MSAgent.repository.CompetenceAgentRepository;
import org.ressourcesarea.MSAgent.service.contract.CompetenceAgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CompetenceAgentServiceImpl implements CompetenceAgentService {

    // ----- Injections de dépendances ----- //

    private final CompetenceAgentRepository competenceAgentRepository;
    private final CompetenceAgentMapper competenceAgentMapper;
    private final AgentRepository agentRepository;
    private final AgentMapper agentMapper;

    @Autowired
    public CompetenceAgentServiceImpl(CompetenceAgentRepository competenceAgentRepository,
                                      CompetenceAgentMapper competenceAgentMapper,
                                      AgentRepository agentRepository,
                                      AgentMapper agentMapper){
        this.competenceAgentRepository = competenceAgentRepository;
        this.competenceAgentMapper = competenceAgentMapper;
        this.agentRepository = agentRepository;
        this.agentMapper = agentMapper;
    }

    // ----- Méthodes ----- //

    /**
     * {@inheritDoc}
     * @param competenceAgentDto
     */
    @Override
    @Transactional
    public void createCompetenceAgent(final CompetenceAgentDto competenceAgentDto) {
        competenceAgentRepository.save(
                competenceAgentMapper.competenceUtilisateurDtoToCompetenceUtilisateur(competenceAgentDto));
    }

    /**
     * {@inheritDoc}
     * @param competenceAgentDtoList
     */
    @Override
    @Transactional
    public void updateCompetenceAgent(final List<CompetenceAgentDto> competenceAgentDtoList, String matricule) {
        try {
            AgentDto agentDto = agentMapper.fromEntityToDto(agentRepository.getAgentByMatricule(matricule));
            if (agentDto !=null) {
                // On supprime les compétences de l'agent avant d'en rajouter suivant
                // ce qu'il a indiqué
                List<CompetenceAgentDto> oldCompetenceAgentList = competenceAgentMapper.mapToDto(
                        competenceAgentRepository.getCompetenceAgentByMatriculeAgent(matricule));
                for (CompetenceAgentDto oldCompetenceAgent : oldCompetenceAgentList) {
                    competenceAgentRepository.delete(
                            competenceAgentMapper.competenceUtilisateurDtoToCompetenceUtilisateur(
                                    oldCompetenceAgent
                            ));
                }

                // On ajoute les nouvelles compétences indiqué
                for (CompetenceAgentDto competenceAgentDto : competenceAgentDtoList) {
                    if (competenceAgentDto.getFormateur() == null) {
                        competenceAgentDto.setFormateur(false);
                        competenceAgentDto.setAgent(agentMapper.fromDtoToEntity(agentDto));
                    }
                    competenceAgentRepository.save(
                            competenceAgentMapper.competenceUtilisateurDtoToCompetenceUtilisateur(
                                    competenceAgentDto));
                }
            } else {
                throw new EntityException("L'agent n'existe pas");
            }
        } catch (EntityException e) {
            throw new EntityException("Erreur lors de la modification des compétences d'un agent");
        }
    }

    /**
     * {@inheritDoc}
     * @param matricule
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<CompetenceAgentDto> getCompetenceAgentByMatriculeAgent(final String matricule) {
        return competenceAgentMapper.mapToDto(
                competenceAgentRepository.getCompetenceAgentByMatriculeAgent(matricule)
        );
    }

}
