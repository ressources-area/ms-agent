package org.ressourcesarea.MSAgent.service.contract;

import org.ressourcesarea.MSAgent.model.dtos.CentreDto;

import java.util.List;

public interface CentreService {

    /**
     * Create Centre
     */

    /**
     * Get Centre
     */
    CentreDto getCentreByNom(String nom);

    /**
     * Get all centre
     * @return
     */
    List<CentreDto> getAllCentre();

    /**
     * Update Centre
     */

    /**
     * Delete Centre
     */
}
