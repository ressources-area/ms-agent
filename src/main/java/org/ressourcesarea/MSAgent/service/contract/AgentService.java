package org.ressourcesarea.MSAgent.service.contract;

import org.ressourcesarea.MSAgent.model.dtos.AgentDto;
import org.ressourcesarea.MSAgent.model.dtos.RegistrerDto;

import java.util.List;

public interface AgentService {

    /**
     * Récupère la liste de tous les agents
     */
    List<AgentDto> getListAgent();

    /**
     * Récupère les données pour afficher le profil de l'agent
     * @param matricule
     * @return
     */
    AgentDto getProfil(String matricule);

    /**
     * Récupère l'agent selon son id
     * @param idAgent
     * @return
     */
    AgentDto getAgentById(int idAgent);

    /**
     * Récupère l'agent selon son matricule
     * @param matricule
     * @return
     */
    AgentDto getAgentByMatricule(String matricule);

    /**
     * Récupérer la liste des agents d'un centre
     * @return une liste d'agents
     */
    List<AgentDto> getAgentByCentre(String nomCentre);

    /**
     * Récupérer une liste selon le saisie
     * @param nomPrenom
     * @return
     */
    List<AgentDto> getAgentListBySearch(String nomPrenom);

    /**
     * Récupère la liste des agents non présent dans la liste de stage
     * @param matriculeList
     * @return
     */
    List<AgentDto> getAgentNotInStage(List<String> matriculeList);

    /**
     * Créer un agent
     */
    void createAgent(RegistrerDto registrerDto);

    /**
     * Modifier un profil
     */
    void updateAgent(AgentDto agentDto);

    /**
     * Récupère la liste des agents avec la compétence demandée
     * @param intituleCompetence
     * @return une liste d'agent
     */
    List<AgentDto> getAgentByCompetenceAndFormateur(String intituleCompetence);

    /**
     * Update Password
     */
    void updatePassword(AgentDto agentDto, String oldPassword, String newPassowrd);

    /**
     * Changer le role de l'agent
     * Si ADMIN -> USER
     * Si USER -> ADMIN
     * @param agentDto
     */
    void changeRole(AgentDto agentDto);

    /**
     * Delete Agent
     */
    void deleteAgent(Integer idAgent);
}
