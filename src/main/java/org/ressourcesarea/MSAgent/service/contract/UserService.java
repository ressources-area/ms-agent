package org.ressourcesarea.MSAgent.service.contract;

import org.ressourcesarea.MSAgent.model.dtos.LoginRequest;
import org.ressourcesarea.MSAgent.model.dtos.LoginResponse;
import org.ressourcesarea.MSAgent.model.dtos.PrincipaleAgentDto;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface UserService {
    ResponseEntity<LoginResponse> login(LoginRequest loginRequest, String accessToken, String refreshToken);

    ResponseEntity<LoginResponse> refresh(String accessToken, String refreshToken);

    PrincipaleAgentDto getUserProfile();

    String logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse);
}
