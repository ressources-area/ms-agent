package org.ressourcesarea.MSAgent.service.contract;

import org.ressourcesarea.MSAgent.model.dtos.CompetenceAgentDto;

import java.util.List;

public interface CompetenceAgentService {

    /**
     * Create
     */
    void createCompetenceAgent(CompetenceAgentDto competenceAgentDto);

    /**
     * Get CompetenceAgentByIdAgent
     */
    List<CompetenceAgentDto> getCompetenceAgentByMatriculeAgent(String matricule);

    /**
     * Update
     */
    void updateCompetenceAgent(List<CompetenceAgentDto> competenceAgentDtoList, String matricule);

    /**
     * Delete
     */
}
