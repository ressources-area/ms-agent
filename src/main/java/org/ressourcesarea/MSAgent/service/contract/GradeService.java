package org.ressourcesarea.MSAgent.service.contract;

import org.ressourcesarea.MSAgent.model.dtos.GradeDto;

import java.util.List;

public interface GradeService {

    /**
     * Create
     */

    /**
     * Get Grade
     */
    GradeDto getGradeByIntitule(String intitule);

    /**
     * Get all Grade
     */
    List<GradeDto> getAllGrade();

    /**
     * Update
     */

    /**
     * Delete
     */
}
