package org.ressourcesarea.MSAgent.service.contract;

import org.ressourcesarea.MSAgent.model.dtos.CompetenceDto;

import java.util.List;

public interface CompetenceService {

    /**
     * Create
     */

    /**
     * Get Competence
     */
    CompetenceDto getCompetenceByCompetence(String competence);

    /**
     * Get all Competence
     */
    List<CompetenceDto> getAllCompetence();

    /**
     * Update
     */

    /**
     * Delete
     */
}
