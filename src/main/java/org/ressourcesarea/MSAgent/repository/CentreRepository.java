package org.ressourcesarea.MSAgent.repository;

import org.ressourcesarea.MSAgent.model.entities.Centre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CentreRepository extends JpaRepository<Centre,Long> {

    @Query("SELECT c FROM Centre c WHERE c.nom = :nomCentre")
    Centre getCentreByNom(@Param("nomCentre") String nomCentre);

    @Query("SELECT c FROM Centre c")
    List<Centre> getAllCentre();
}
