package org.ressourcesarea.MSAgent.repository;

import org.ressourcesarea.MSAgent.model.entities.Grade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GradeRepository extends JpaRepository<Grade, Long> {

    @Query("SELECT g FROM Grade g WHERE g.intitule = :intituleGrade")
    Grade getGradeByIntitule(@Param("intituleGrade") String intituleGrade);

    @Query("SELECT g FROM Grade g")
    List<Grade> getAllGrade();
}
