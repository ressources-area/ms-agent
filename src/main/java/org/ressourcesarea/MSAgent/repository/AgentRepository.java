package org.ressourcesarea.MSAgent.repository;

import org.ressourcesarea.MSAgent.model.entities.Agent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface AgentRepository extends JpaRepository<Agent, Long> {

    /**
     * Récupère la liste de tous les agents
     * @return
     */
    @Query("SELECT a FROM Agent a")
    List<Agent> getListAgent();

    /**
     * Récupère un agent selon son id
     * @param idAgent
     * @return
     */
    @Query("SELECT a FROM Agent a WHERE a.id =:idAgent")
    Agent getAgentById(@Param("idAgent") Integer idAgent);

    /**
     * Récupère un agent selon son matricule
     */
    @Query("SELECT a FROM Agent a WHERE a.matricule LIKE :matricule")
    Agent getAgentByMatricule(@Param("matricule") String matricule);

    /**
     * Récupère les agents selon le centre
     * @param nomCentre
     * @return
     */
    @Query("SELECT a FROM Agent a WHERE a.centre.nom = :nomCentre")
    List<Agent> getAgentByCentre(@Param("nomCentre") String nomCentre);

    /**
     * Récupère les agents selon leurs compétences de formateur
     * @param intituleCompetence
     * @return
     */
    @Query("SELECT c.agent FROM CompetenceAgent c " +
            "WHERE (c.competence.intitule = :intituleCompetence) AND " +
            "(c.formateur = TRUE ) " +
            "GROUP BY c.agent")
    List<Agent> getAgentByCompetenceAndFormateur(
            @Param("intituleCompetence") String intituleCompetence);

    /**
     * Récupère la liste des agents selon la rrcherche
     * @param nomPrenom
     * @return
     */
    @Query("SELECT a FROM Agent a WHERE (a.nom LIKE :nomPrenom) OR (a.prenom LIKE :nomPrenom)")
    List<Agent> getAgentListBySearch(@Param("nomPrenom") String nomPrenom);

    /**
     * Récupère un agent selon le matricule
     * @param matricule
     * @return
     */
    @Query("SELECT a FROM Agent a WHERE a.matricule= :matricule")
    Optional<Agent> findAgentWithMatricule(@Param("matricule") String matricule);

    /**
     * Récupère un agent selon matricule
     * @param matricule
     * @return
     */
    Optional<Agent> findByMatricule(String matricule);

    @Modifying
    @Transactional
    @Query("UPDATE Agent a SET a.role= :role WHERE a.id =:idAgent")
    void updateRole(@Param("role") String role,
                    @Param("idAgent") Integer idAgent);


}
