package org.ressourcesarea.MSAgent.repository;

import org.ressourcesarea.MSAgent.model.entities.Competence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompetenceRepository extends JpaRepository<Competence,Long> {

    @Query("SELECT c FROM Competence c WHERE c.intitule = :intituleCompetence")
    Competence getCompetenceByCompetence(@Param("intituleCompetence") String intituleCompetence);

    @Query("SELECT c FROM Competence c")
    List<Competence> getAllCompetence();
}
