package org.ressourcesarea.MSAgent.repository;

import org.ressourcesarea.MSAgent.model.entities.CompetenceAgent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompetenceAgentRepository extends JpaRepository <CompetenceAgent, Long> {

    /* Récupère la liste des compétences d'un agent */
    @Query("SELECT c FROM CompetenceAgent c WHERE c.agent.matricule= :matricule")
    List<CompetenceAgent> getCompetenceAgentByMatriculeAgent(@Param("matricule") String matricule);

}
