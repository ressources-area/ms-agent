package org.ressourcesarea.MSAgent.application.controller;

import org.ressourcesarea.MSAgent.model.dtos.CompetenceDto;
import org.ressourcesarea.MSAgent.service.contract.CompetenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/competence")
public class CompetenceController {

    // ----- Injections des dépendances ----- //

    private final CompetenceService competenceService;

    @Autowired
    public CompetenceController(CompetenceService competenceService) {
        this.competenceService = competenceService;
    }

    // ----- Méthodes ----- //

    @GetMapping(value = "/getAllCompetence")
    public List<CompetenceDto> getAllCompetence(){
        try {
            return competenceService.getAllCompetence();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors de la récupération des compétences"
            );
        }
    }
}
