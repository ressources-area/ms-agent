package org.ressourcesarea.MSAgent.application.controller;

import org.ressourcesarea.MSAgent.model.dtos.CentreDto;
import org.ressourcesarea.MSAgent.service.contract.CentreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/centre")
public class CentreController {

    // ----- Injections des dépendances ----- //

    private final CentreService centreService;

    @Autowired
    public CentreController(CentreService centreService){
        this.centreService = centreService;
    }

    // ----- Méthodes ----- //

    /**
     * Récupère tout les centres
     * @return
     */
    @GetMapping(value = "/getAllCentre")
    public List<CentreDto> getAllCentre() {
        try {
            return centreService.getAllCentre();
        } catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors de la récupération des centres"
            );
        }
    }
}
