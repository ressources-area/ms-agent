package org.ressourcesarea.MSAgent.application.controller;

import org.ressourcesarea.MSAgent.model.dtos.GradeDto;
import org.ressourcesarea.MSAgent.service.contract.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/grade")
public class GradeController {

    // ----- Injections des dépendances ----- //

    private final GradeService gradeService;

    @Autowired
    public GradeController(GradeService gradeService){
        this.gradeService = gradeService;
    }

    // ----- Méthodes ----- //

    @GetMapping(value = "/getAllGrade")
    public List<GradeDto> getAllGrade(){
        try {
            return gradeService.getAllGrade();
        } catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors de la récupération des grades"
            );
        }
    }

}
