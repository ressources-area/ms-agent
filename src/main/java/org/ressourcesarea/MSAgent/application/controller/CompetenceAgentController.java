package org.ressourcesarea.MSAgent.application.controller;

import org.ressourcesarea.MSAgent.model.dtos.CompetenceAgentDto;
import org.ressourcesarea.MSAgent.service.contract.CompetenceAgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/competenceAgent")
public class CompetenceAgentController {

    // ----- Injections des dépendances ----- //

    private final CompetenceAgentService competenceAgentService;

    @Autowired
    public CompetenceAgentController(CompetenceAgentService competenceAgentService){
        this.competenceAgentService = competenceAgentService;
    }

    // ----- Méthodes ----- //

    /**
     * Récupère les compétences de l'agent selon son matricule
     * @param matricule
     * @return
     */
    @GetMapping(value = "/{matricule}")
    public List<CompetenceAgentDto> getCompetenceByIdAgent(@PathVariable("matricule") String matricule){
        try {
            return competenceAgentService.getCompetenceAgentByMatriculeAgent(matricule);
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur de la récupération des compétences d'agent selon le matricule"
            );
        }
    }

    /**
     * Modifie les compétences de l'agent
     * @param competenceAgentDtoList
     */
    @PutMapping(value = "/update/{matricule}")
    public void updateCompetenceAgent(
            @RequestBody List<CompetenceAgentDto> competenceAgentDtoList,
            @PathVariable("matricule") String matricule){
        competenceAgentService.updateCompetenceAgent(competenceAgentDtoList, matricule);
    }

}
