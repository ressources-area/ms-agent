package org.ressourcesarea.MSAgent.application.controller;

import org.ressourcesarea.MSAgent.application.error.AuthorizeException;
import org.ressourcesarea.MSAgent.application.error.EntityException;
import org.ressourcesarea.MSAgent.application.error.GeneralException;
import org.ressourcesarea.MSAgent.model.dtos.AgentDto;
import org.ressourcesarea.MSAgent.model.dtos.PrincipaleAgentDto;
import org.ressourcesarea.MSAgent.model.dtos.RegistrerDto;
import org.ressourcesarea.MSAgent.service.contract.AgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/agent")
public class AgentController {

    // ----- Injections des dépendances ----- //

    private final AgentService agentService;
    private final AuthController authController;

    @Autowired
    public AgentController(AgentService agentService,
                           AuthController authController) {
        this.agentService = agentService;
        this.authController = authController;
    }

    // ----- Méthodes ----- //


    @GetMapping(value = "/listAgent")
    public List<AgentDto> getAgentList() {
        try {
            return agentService.getListAgent();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors de la récupération de la liste d'agent"
            );
        }
    }

    /**
     * Récupère le profil
     *
     * @param matricule
     * @return
     */
    @GetMapping(value = "/profil/{matricule}")
    public AgentDto getProfil(@PathVariable("matricule") String matricule) {
        try {
            SecurityContext context = SecurityContextHolder.getContext();
            PrincipaleAgentDto principaleAgentDto = (PrincipaleAgentDto) context.getAuthentication().getPrincipal();
            if (matricule.equals(principaleAgentDto.getMatricule()) || principaleAgentDto.getRole().equals("ADMIN")) {
                return agentService.getProfil(matricule);
            }
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED, "Vous n'avez pas accès à cette page");
        } catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors de la récupération du profil"
            );
        }

    }

    /**
     * Récupère la liste d'agent selon le centre
     *
     * @param nomCentre
     * @return List d'Agent
     */
    @GetMapping(value = "/agentList")
    public List<AgentDto> getAgentByCentre(@RequestParam(name = "nomCentre") String nomCentre) {
        try {
            return agentService.getAgentByCentre(nomCentre);
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors la récupération de la liste d'agent selon le centre"
            );
        }
    }

    /**
     * Récupère une liste d'agent selon ses compétences de formateur
     *
     * @param intituleCompetence
     * @return List
     */
    @GetMapping(value = "/agentListFormateur")
    public List<AgentDto> getAgentListByFormateurCompetence(
            @RequestParam(name = "intituleCompetence") String intituleCompetence) {
        try {
            return agentService.getAgentByCompetenceAndFormateur(intituleCompetence);
        } catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors la récupération des agents selon la compétence de formateur"
            );
        }
    }

    @GetMapping(value = "/searchAgent")
    public List<AgentDto> getAgentListBySearch(@RequestParam("nomPrenom") String nomPrenom){
        try {
            return agentService.getAgentListBySearch(nomPrenom);
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors la récupération des agents selon le nom et prénom"
            );
        }
    }

    /**
     * Ajoute un nouvel agent
     * @return un agent
     */
    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void createAgent(@Valid @RequestBody RegistrerDto registrerDto) {
        try {
            agentService.createAgent(registrerDto);
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors de la création d'un nouvel agent"
            );
        }
    }

    /**
     * Récupère un agent par son matricule
     *
     * @param matricule
     * @return
     */
    @GetMapping("/getAgent/{matricule}")
    public AgentDto getAgentByMatricule(@PathVariable("matricule") String matricule) {
        try {
            return agentService.getAgentByMatricule(matricule);
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la récupération de l'agent selon son matricule"
            );
        }
    }

    /**
     * Récupère la liste des agents qui ne sont pas dans le stage
     * @return
     */
    @GetMapping("/getAgentListInNotStage")
    public List<AgentDto> getAgentNotInStage(@RequestParam("matriculeList") List<String> matriculeList){
        List<AgentDto> agentDtoList = agentService.getAgentNotInStage(matriculeList);
        return agentDtoList;
    }

    /**
     * Modifie le profil d'un agent
     *
     * @param agentDto
     * @return un agent
     */
    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateAgent(@RequestBody AgentDto agentDto) {
        try {
            AgentDto agentConnected = authController.getAgentConnected();
            if (agentConnected.getId().equals(agentDto.getId())){
                agentService.updateAgent(agentDto);
            } else {
                throw new EntityException("Vous n'ếtes pas authorisé à effectuer la modification de cet agent");
            }
        }
        catch (GeneralException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la modification du profil", e
            );
        }
    }

    /**
     * Suppression d'un agent
     *
     * @param idAgent
     */
    @DeleteMapping(value = "/delete/{idAgent}")
    public void deleteAgent(@PathVariable("idAgent") Integer idAgent) {
        try {
            AgentDto agentConnected = authController.getAgentConnected();
            if(agentConnected.getRole().equals("ADMIN")){
                agentService.deleteAgent(idAgent);
            } else {
                throw new AuthorizeException("Vous n'ếtes pas authorisé à supprimer un agent");
            }
        }
        catch (GeneralException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la suppression de l'agent", e
            );
        }
    }

    /**
     * Change le role de l'agent
     * @param agentDto
     */
    @PutMapping(value = "/changeRole")
    public void changeRole(@RequestBody AgentDto agentDto){
        try {
            AgentDto agentConnected = authController.getAgentConnected();
            if (agentConnected.getRole().equals("ADMIN")){
                agentService.changeRole(agentDto);
            } else {
                throw new EntityException("Vous n'ếtes pas authorisé à effectuer un changement de role");
            }
        } catch (GeneralException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors du changement de role",e
            );
        }
    }

    /**
     * Modifier le mot de passe
     * @param agentDto
     * @param newPassword
     * @param oldPassword
     */
    @PutMapping(value = "/updatePassword")
    public void updatePassword(@RequestBody AgentDto agentDto,
                               @RequestParam("newPassword") String newPassword,
                               @RequestParam("oldPassword") String oldPassword){
        try {
            AgentDto agentConnected = authController.getAgentConnected();
            if(agentConnected.getId().equals(agentDto.getId())){
                agentService.updatePassword(agentDto, oldPassword, newPassword);
            } else {
                throw new EntityException("Vous n'êtes pas authorisé à effectuer la modification");
            }
        }catch (AuthorizeException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur lors de la modification du password",e
            );
        }
    }
}
