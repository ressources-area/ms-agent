package org.ressourcesarea.MSAgent.application.controller;

import org.ressourcesarea.MSAgent.Utils.security.SecurityCipher;
import org.ressourcesarea.MSAgent.application.error.AuthorizeException;
import org.ressourcesarea.MSAgent.model.dtos.*;
import org.ressourcesarea.MSAgent.security.JwtTokenProvider;
import org.ressourcesarea.MSAgent.service.contract.AgentService;
import org.ressourcesarea.MSAgent.service.contract.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("/auth")
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final AgentService agentService;
    private final JwtTokenProvider jwtTokenProvider;


    @Autowired
    public AuthController(UserService userService,
                          AuthenticationManager authenticationManager,
                          AgentService agentService,
                          JwtTokenProvider jwtTokenProvider){
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.agentService = agentService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginResponse> login(
            @CookieValue(name = "accessToken",required = false) String accessToken,
            @CookieValue(name = "refreshToken",required = false) String refreshToken,
            @Valid @RequestBody LoginRequest loginRequest
            ) {
        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    loginRequest.getMatricule(), loginRequest.getPassword()
            ));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String decryptedAccessToken = SecurityCipher.decrypt(accessToken);
            String decryptedRefreshToken = SecurityCipher.decrypt(refreshToken);
            return userService.login(loginRequest, decryptedAccessToken, decryptedRefreshToken);
        } catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors de l'authentication"
            );
        }
    }

    @PostMapping(value = "/refresh", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginResponse> refreshToken(
            @CookieValue(name = "accessToken", required = false) String accessToken,
            @CookieValue(name = "refreshToken", required = false) String refreshToken){

        try {
            String decryptedAccessToken = SecurityCipher.decrypt(accessToken);
            String decryptedRefreshToken = SecurityCipher.decrypt(refreshToken);
            return userService.refresh(decryptedAccessToken,decryptedRefreshToken);
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Erreur lors du refresh du token"
            );
        }
    }

    @GetMapping(value = "/me", produces = MediaType.APPLICATION_JSON_VALUE)
    public AgentDto getAgentConnected(){
        try{
            SecurityContext context = SecurityContextHolder.getContext();
            PrincipaleAgentDto principal = (PrincipaleAgentDto) context.getAuthentication().getPrincipal();
            String matricule = principal.getMatricule();
            return agentService.getAgentByMatricule(matricule);
        } catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Vous n'êtes pas authorisé"
            );
        }
    }

    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> logout(HttpServletRequest request, HttpServletResponse response){
        try {
            return new ResponseEntity(new ApiResponseMessage(true,userService.logout(
                    request,response)), HttpStatus.OK);
        } catch (AuthorizeException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,"Vous n'ếtes pas authorisé à accéder à logout"
            );
        }
    }

    @GetMapping(value = "/authSuccess")
    Boolean authSuccess(@RequestParam("token") String token){
        try {
            String newToken = SecurityCipher.decrypt(token);
            return jwtTokenProvider.validateToken(newToken);
        } catch (AuthorizeException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Erreur de demande d'authSuccess"
            );
        }
    }

}
