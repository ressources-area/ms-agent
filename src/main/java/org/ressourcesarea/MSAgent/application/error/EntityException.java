package org.ressourcesarea.MSAgent.application.error;

public class EntityException extends RuntimeException {
    public EntityException(String message){
        super(message);
    }
}
