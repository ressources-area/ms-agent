package org.ressourcesarea.MSAgent.application.error;

public class GeneralException extends RuntimeException {
    public GeneralException(String message){
        super(message);
    }
}
