package org.ressourcesarea.MSAgent.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.ressourcesarea.MSAgent.model.dtos.CompetenceDto;
import org.ressourcesarea.MSAgent.model.entities.Competence;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CompetenceMapper {

    CompetenceMapper INSTANCE = Mappers.getMapper(CompetenceMapper.class);

    List<CompetenceDto> mapToDto(List<Competence> competences);
    List<Competence> map(List<CompetenceDto> competenceDtos);
    CompetenceDto competenceToCompetenceDto(Competence competence);
    Competence competenceDtoToCompetence(CompetenceDto competenceDto);
}
