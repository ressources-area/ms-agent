package org.ressourcesarea.MSAgent.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.ressourcesarea.MSAgent.model.dtos.AgentDto;
import org.ressourcesarea.MSAgent.model.entities.Agent;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AgentMapper {

    List<AgentDto> mapToDto(List<Agent> agents);
    List<Agent> map(List<AgentDto> agentDtos);
    AgentDto fromEntityToDto(Agent agent);
    Agent fromDtoToEntity(AgentDto agentDto);
}
