package org.ressourcesarea.MSAgent.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.ressourcesarea.MSAgent.model.dtos.GradeDto;
import org.ressourcesarea.MSAgent.model.entities.Grade;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GradeMapper {

    GradeMapper INSTANCE = Mappers.getMapper(GradeMapper.class);

    List<GradeDto> mapGradeDtos(List<Grade> grades);
    List<Grade> map(List<GradeDto> gradeDtos);
    GradeDto gradeToGradeDto(Grade grade);
    Grade gradeDtoToGrade(GradeDto gradeDto);
}
