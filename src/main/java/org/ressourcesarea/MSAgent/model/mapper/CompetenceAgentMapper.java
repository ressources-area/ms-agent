package org.ressourcesarea.MSAgent.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.ressourcesarea.MSAgent.model.dtos.CompetenceAgentDto;
import org.ressourcesarea.MSAgent.model.entities.CompetenceAgent;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CompetenceAgentMapper {

    CompetenceAgentMapper INSTANCE = Mappers.getMapper(CompetenceAgentMapper.class);

    List<CompetenceAgentDto> mapToDto(List<CompetenceAgent> competenceAgents);
    List<CompetenceAgent> map(List<CompetenceAgentDto> competenceAgentDtos);
    CompetenceAgentDto competenceUtilisateurToCompetenceUtilisateurDto(
            CompetenceAgent competenceAgent
    );
    CompetenceAgent competenceUtilisateurDtoToCompetenceUtilisateur(
            CompetenceAgentDto competenceAgentDto);
}
