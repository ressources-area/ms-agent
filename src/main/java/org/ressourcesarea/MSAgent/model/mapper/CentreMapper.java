package org.ressourcesarea.MSAgent.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.ressourcesarea.MSAgent.model.dtos.CentreDto;
import org.ressourcesarea.MSAgent.model.entities.Centre;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CentreMapper {

    CentreMapper INSTANCE = Mappers.getMapper(CentreMapper.class);

    List<CentreDto> mapToDto(List<Centre> centres);
    List<Centre> map(List<CentreDto> centreDtos);
    CentreDto centreToCentreDto(Centre centre);
    Centre centreDtoToCentre(CentreDto centreDto);
}
