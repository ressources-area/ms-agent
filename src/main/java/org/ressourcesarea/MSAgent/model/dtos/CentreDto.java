package org.ressourcesarea.MSAgent.model.dtos;

import lombok.Data;

@Data
public class CentreDto {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String nom;
    private String adresse;
    private String codePostale;
    private String ville;
}
