package org.ressourcesarea.MSAgent.model.dtos;

import lombok.Data;
import org.ressourcesarea.MSAgent.model.entities.Agent;

@Data
public class CompetenceAgentDto {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Boolean formateur;
    private Agent agent;
    private CompetenceDto competence;
}
