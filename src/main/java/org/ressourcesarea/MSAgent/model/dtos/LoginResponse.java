package org.ressourcesarea.MSAgent.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginResponse {

    private SuccessFailure status;
    private String message;
    private String error;
    private String token;

    public enum SuccessFailure {
        SUCCESS, FAILURE
    }

    public LoginResponse(SuccessFailure status, String message, String token) {
        this.status = status;
        this.message = message;
        this.token = token;
    }
}
