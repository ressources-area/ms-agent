package org.ressourcesarea.MSAgent.model.dtos;

import lombok.Data;

@Data
public class AgentDto {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String prenom;
    private String nom;
    private String matricule;
    private String role;
    private GradeDto grade;
    private CentreDto centre;
}
