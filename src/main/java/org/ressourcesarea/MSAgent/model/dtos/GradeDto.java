package org.ressourcesarea.MSAgent.model.dtos;

import lombok.Data;

@Data
public class GradeDto {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String intitule;
}
