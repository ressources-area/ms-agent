package org.ressourcesarea.MSAgent.model.dtos;

import lombok.Data;

@Data
public class RegistrerDto {

    private Integer id;
    private String prenom;
    private String nom;
    private String matricule;
    private String password;
    private String confirmationPassword;
    private String role;
    private GradeDto grade;
    private CentreDto centre;
}
