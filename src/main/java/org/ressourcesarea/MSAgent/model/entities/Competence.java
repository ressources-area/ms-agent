package org.ressourcesarea.MSAgent.model.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "competence")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Competence implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "intitule", nullable = false)
    private String intitule;

//    @JsonIgnore
//    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "competence_agent", joinColumns = @JoinColumn(name = "competence_id"),
//            inverseJoinColumns = @JoinColumn (name = "agent_id"))
//    private List<Agent> agentList = new ArrayList<>();
}
