package org.ressourcesarea.MSAgent.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.ressourcesarea.MSAgent.application.error.EntityException;
import org.ressourcesarea.MSAgent.model.dtos.AgentDto;
import org.ressourcesarea.MSAgent.model.dtos.CentreDto;
import org.ressourcesarea.MSAgent.model.dtos.GradeDto;
import org.ressourcesarea.MSAgent.model.dtos.RegistrerDto;
import org.ressourcesarea.MSAgent.model.mapper.AgentMapper;
import org.ressourcesarea.MSAgent.repository.AgentRepository;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

@RunWith(MockitoJUnitRunner.class)
public class ValidationServiceTest {

    @InjectMocks
    ValidationService validationService;

    @Mock
    private AgentRepository agentRepository;

    @Mock
    private AgentMapper agentMapper;

    private CentreDto createCentre(){
        CentreDto centreDto = new CentreDto();
        centreDto.setId(1);
        centreDto.setAdresse("AdresseTest");
        centreDto.setVille("VilleTest");
        centreDto.setNom("NomTest");
        centreDto.setCodePostale("55555");
        return centreDto;
    }

    private GradeDto createGrade(){
        GradeDto gradeDto = new GradeDto();
        gradeDto.setId(1);
        gradeDto.setIntitule("TestIntitule");
        return gradeDto;
    }

    private AgentDto createAgent(){
        AgentDto agentDto = new AgentDto();
        agentDto.setId(1);
        agentDto.setNom("TestNom");
        agentDto.setPrenom("TestPrenom");
        agentDto.setMatricule("5555");
        agentDto.setRole("USER");
        agentDto.setCentre(createCentre());
        agentDto.setGrade(createGrade());
        return agentDto;
    }

    private RegistrerDto createRegistrer(){
        RegistrerDto registrerDto = new RegistrerDto();
        registrerDto.setId(1);
        registrerDto.setNom("TestNom");
        registrerDto.setPrenom("TestPrenom");
        registrerDto.setMatricule("5555");
        registrerDto.setPassword("testtest");
        registrerDto.setConfirmationPassword("testtest");
        registrerDto.setRole("USER");
        registrerDto.setCentre(createCentre());
        registrerDto.setGrade(createGrade());
        return registrerDto;
    }

    @Before
    public void setUpMock(){
    }

    @Test
    public void whenPasswordIsNotSame_shouldReturnEntityException() throws EntityException {
        RegistrerDto registrerDto = new RegistrerDto();
        registrerDto.setPassword("testTest");
        registrerDto.setConfirmationPassword("TestTest");

        EntityException exception =
                assertThrows(EntityException.class, () -> validationService.validationCreateUser(registrerDto));

        assertThat(exception.getMessage()).isEqualTo("Les mots de passe ne sont pas identiques");
    }

    @Test
    public void whenPasswordSizeIsUnder6_shouldReturnEntityException() throws Exception{
        RegistrerDto registrerDto = createRegistrer();
        registrerDto.setPassword("test");
        registrerDto.setConfirmationPassword("test");

        EntityException exception =
                assertThrows(EntityException.class, () -> validationService.validationCreateUser(registrerDto));

        assertThat(exception.getMessage()).isEqualTo("Le mot de passe doit contenir au minimum 6 caractères");
    }

    @Test
    public void whenMatriculeSizeIsUnder4_shouldReturnEntityException() throws Exception{
        RegistrerDto registrerDto = createRegistrer();
        registrerDto.setMatricule("445");


    }

    @Test
    public void whenCheckPasswordSizeIsUnder6_shouldReturnEntityException() throws Exception{
        try{
            String newPassword = "test";
            validationService.checkPasswordValid(newPassword);
        } catch (EntityException e){
            assertEquals("Le mot de passe doit contenir au minimum 6 caractère",e.getMessage());
        }
    }

    @Test
    public void checkGetRoleIsNull_shouldReturnEntityException() throws Exception {
        AgentDto agentDto = createAgent();
        agentDto.setRole(null);

        EntityException exception =
                assertThrows(EntityException.class, () -> validationService.checkAgentValid(agentDto));

        assertThat(exception.getMessage()).isEqualTo("Veuillez remplir tout les champs obligatoires");

    }

    @Test
    public void checkAgentValid_shouldReturnTrue(){
        RegistrerDto registrerDto = createRegistrer();
        assertEquals(validationService.validationCreateUser(registrerDto),true);
    }

    @Test
    public void checkValidationCreateUser_shouldReturnTrue(){
        AgentDto agentDto = createAgent();
        assertEquals(validationService.checkAgentValid(agentDto), true);
    }

}
