package org.ressourcesarea.MSAgent.integrationTestEndPoints;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.ressourcesarea.MSAgent.model.dtos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.Cookie;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class CompetenceAgentIntegrationTest {

    @Autowired
    private MockMvc mockMvc;


    private CentreDto createCentre() {
        CentreDto centreDto = new CentreDto();
        centreDto.setId(1);
        centreDto.setAdresse("AdresseTest");
        centreDto.setNom("NomTest");
        centreDto.setVille("VilleTest");
        centreDto.setCodePostale("55555");
        return centreDto;
    }

    private GradeDto createGrade() {
        GradeDto gradeDto = new GradeDto();
        gradeDto.setId(1);
        gradeDto.setIntitule("IntituleTest");
        return gradeDto;
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getTokenAdmin() throws Exception {
        String token;

        LoginRequest loginRequest = new LoginRequest("1234", "admin");

        MvcResult mvcResult = mockMvc.perform(post("/auth/login")
                .header("origin", "origin")
                .content(asJsonString(loginRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();
        token = mvcResult.getResponse().getCookie("AuthToken").getValue();

        return token;
    }

    @Test
    void getCompetenceByIdAgent() throws Exception {
        String token = getTokenAdmin();

        mockMvc.perform(get("/competenceAgent/{matricule}","4821")
                .header("origin","origin")
                .cookie(new Cookie("AuthToken",token)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].formateur", Matchers.is(true)));
    }

    @Test
    void updateCompetenceAgent() throws Exception {
        String token = getTokenAdmin();

        CompetenceDto competenceDto = new CompetenceDto();
        competenceDto.setId(1);
        competenceDto.setIntitule("SUAP");

        AgentDto agentDto = new AgentDto();
        agentDto.setRole("ADMIN");

        CompetenceAgentDto competenceAgentDto = new CompetenceAgentDto();
        competenceAgentDto.setCompetence(competenceDto);
        List<CompetenceAgentDto> competenceAgentDtoList = new ArrayList<>();
        competenceAgentDtoList.add(competenceAgentDto);

        mockMvc.perform(put("/competenceAgent/update/{matricule}","1234")
        .header("origin","origin")
        .cookie(new Cookie("AuthToken",token))
        .content(asJsonString(competenceAgentDtoList))
        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void updateCompetenceAgent_whenMatriculeNotExist_shouldReturnEntityException() throws Exception {
        String token = getTokenAdmin();

        CompetenceDto competenceDto = new CompetenceDto();
        competenceDto.setId(1);
        competenceDto.setIntitule("SUAP");

        AgentDto agentDto = new AgentDto();
        agentDto.setRole("ADMIN");

        CompetenceAgentDto competenceAgentDto = new CompetenceAgentDto();
        competenceAgentDto.setCompetence(competenceDto);
        List<CompetenceAgentDto> competenceAgentDtoList = new ArrayList<>();
        competenceAgentDtoList.add(competenceAgentDto);

        mockMvc.perform(put("/competenceAgent/update/{matricule}","8878")
            .header("origin","origin")
            .cookie(new Cookie("AuthToken",token))
            .content(asJsonString(competenceAgentDtoList))
            .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.error",
                        is("Erreur lors de la modification des compétences d'un agent")));
    }
}
