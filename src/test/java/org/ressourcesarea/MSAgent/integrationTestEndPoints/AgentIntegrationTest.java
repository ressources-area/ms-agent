package org.ressourcesarea.MSAgent.integrationTestEndPoints;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.ressourcesarea.MSAgent.model.dtos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.Cookie;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class AgentIntegrationTest {

    @Autowired
    public MockMvc mockMvc;

    private CentreDto createCentre() {
        CentreDto centreDto = new CentreDto();
        centreDto.setId(1);
        centreDto.setAdresse("AdresseTest");
        centreDto.setNom("NomTest");
        centreDto.setVille("VilleTest");
        centreDto.setCodePostale("55555");
        return centreDto;
    }

    private GradeDto createGrade() {
        GradeDto gradeDto = new GradeDto();
        gradeDto.setId(1);
        gradeDto.setIntitule("IntituleTest");
        return gradeDto;
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getTokenAdmin() throws Exception {
        String token;

        LoginRequest loginRequest = new LoginRequest("1234", "admin");

        MvcResult mvcResult = mockMvc.perform(post("/auth/login")
                .header("origin", "origin")
                .content(asJsonString(loginRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();
        token = mvcResult.getResponse().getCookie("AuthToken").getValue();

        return token;
    }

    private String getTokenUser() throws Exception {
        String token;

        LoginRequest loginRequest = new LoginRequest("4821", "admin");

        MvcResult mvcResult = mockMvc.perform(post("/auth/login")
                .header("origin", "origin")
                .content(asJsonString(loginRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();
        token = mvcResult.getResponse().getCookie("AuthToken").getValue();

        return token;
    }

    @BeforeEach
    void init() throws Exception {
    }

    @Test
    void getAgentByCentre() throws Exception {
        String token = getTokenAdmin();

        mockMvc.perform(get("/agent/listAgent")
                .header("origin", "origin")
                .cookie(new Cookie("AuthToken", token)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].nom", Matchers.is("Admin")));
    }

    @Test
    void createAgent() throws Exception {

        RegistrerDto registrerDto = new RegistrerDto();
        registrerDto.setRole("Admin");
        registrerDto.setPrenom("testPrenom");
        registrerDto.setNom("testNom");
        registrerDto.setPassword("testtest");
        registrerDto.setConfirmationPassword("testtest");
        registrerDto.setGrade(createGrade());
        registrerDto.setCentre(createCentre());
        registrerDto.setMatricule("8888");

        mockMvc.perform(MockMvcRequestBuilders
                .post("/agent/create")
                .header("origin", "origin")
                .content(asJsonString(registrerDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void updateAgent() throws Exception {
        String token = getTokenUser();

        AgentDto agentDto = new AgentDto();
        agentDto.setId(2);
        agentDto.setRole("USER");
        agentDto.setPrenom("test");
        agentDto.setNom("test");
        agentDto.setGrade(createGrade());
        agentDto.setCentre(createCentre());
        agentDto.setMatricule("8888");

        mockMvc.perform(MockMvcRequestBuilders
                .put("/agent/update")
                .header("origin", "origin")
                .cookie(new Cookie("AuthToken", token))
                .content(asJsonString(agentDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void updatePassword() throws Exception {
        String token = getTokenAdmin();

        AgentDto agentDto = new AgentDto();
        agentDto.setId(1);

        String oldPassword = "admin";
        String newPassword = "adminTest";

        mockMvc.perform(MockMvcRequestBuilders
        .put("/agent/updatePassword")
                .header("origin","origin")
                .cookie(new Cookie("AuthToken", token))
                .content(asJsonString(agentDto))
                .param("oldPassword",oldPassword)
                .param("newPassword",newPassword)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void updatePasswordWithWrongOldPassword_shouldReturnAuthorizeException() throws Exception {
        String token = getTokenAdmin();

        AgentDto agentDto = new AgentDto();
        agentDto.setId(1);

        String oldPassword = "test";
        String newPassword = "adminTest";

        mockMvc.perform(MockMvcRequestBuilders
                .put("/agent/updatePassword")
                .header("origin","origin")
                .cookie(new Cookie("AuthToken", token))
                .content(asJsonString(agentDto))
                .param("oldPassword",oldPassword)
                .param("newPassword",newPassword)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string(containsString("L'ancien mot de passe n'est pas correct")));
    }

    @Test
    void updatePasswordWithWrongAgentConnected_shouldReturnAuthorizeException() throws Exception {
        String token = getTokenAdmin();

        AgentDto agentDto = new AgentDto();
        agentDto.setId(3);

        String oldPassword = "admin";
        String newPassword = "adminTest";

        mockMvc.perform(MockMvcRequestBuilders
                .put("/agent/updatePassword")
                .header("origin","origin")
                .cookie(new Cookie("AuthToken", token))
                .content(asJsonString(agentDto))
                .param("oldPassword",oldPassword)
                .param("newPassword",newPassword)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.error",is("Vous n'êtes pas authorisé à effectuer la modification")));
    }

    @Test
    void deleteAgent() throws Exception {
        String token = getTokenAdmin();

        mockMvc.perform(MockMvcRequestBuilders
        .delete("/agent/delete/{idAgent}",2)
                .header("origin","origin")
                .cookie(new Cookie("AuthToken", token)))
                .andExpect(status().isOk());
    }

    @Test
    void deleteAgentWithBadId_shouldReturnErreurEntityException() throws Exception {
        String token = getTokenAdmin();

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/agent/delete/{idAgent}",9)
                .header("origin","origin")
                .cookie(new Cookie("AuthToken", token)))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.error",is("L'agent n'existe pas")));

    }

    @Test
    void deleteAgentWithBadRole_shouldReturnAuthorizeException() throws Exception {
        String token = getTokenUser();

        mockMvc.perform(MockMvcRequestBuilders
        .delete("/agent/delete/{idAgent}",2)
        .header("origin","origin")
        .cookie(new Cookie("AuthToken", token)))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.error", is(
                        "Vous n'ếtes pas authorisé à supprimer un agent")));
    }

    @Test
    void changeRole() throws Exception {
        String token = getTokenAdmin();

        AgentDto agentDto = new AgentDto();
        agentDto.setId(1);
        agentDto.setRole("USER");
        agentDto.setPrenom("test");
        agentDto.setNom("test");
        agentDto.setGrade(createGrade());
        agentDto.setCentre(createCentre());
        agentDto.setMatricule("8888");


        mockMvc.perform(MockMvcRequestBuilders
                .put("/agent/changeRole")
                .header("origin", "origin")
                .cookie(new Cookie("AuthToken", token))
                .content(asJsonString(agentDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void changeRoleWithWrongRoleAdmin_shouldReturnEntityException() throws Exception {
        String token = getTokenUser();
        AgentDto agentDto = new AgentDto();
        agentDto.setRole("USER");

        mockMvc.perform(MockMvcRequestBuilders
        .put("/agent/changeRole")
        .header("origin","origin")
        .cookie(new Cookie("AuthToken", token))
        .content(asJsonString(agentDto))
        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.error", is(
                        "Vous n'ếtes pas authorisé à effectuer un changement de role")));
    }
}
