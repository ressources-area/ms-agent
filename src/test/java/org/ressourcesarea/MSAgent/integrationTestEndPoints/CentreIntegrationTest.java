package org.ressourcesarea.MSAgent.integrationTestEndPoints;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.ressourcesarea.MSAgent.model.dtos.LoginRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.Cookie;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class CentreIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getTokenAdmin() throws Exception {
        String token;

        LoginRequest loginRequest = new LoginRequest("1234", "admin");

        MvcResult mvcResult = mockMvc.perform(post("/auth/login")
                .header("origin", "origin")
                .content(asJsonString(loginRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();
        token = mvcResult.getResponse().getCookie("AuthToken").getValue();

        return token;
    }

    @Test
    void getCentre() throws Exception {
        String token = getTokenAdmin();

        mockMvc.perform(get("/centre/getAllCentre")
        .header("origin","origin")
        .cookie(new Cookie("AuthToken",token)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(3)));
    }
}
