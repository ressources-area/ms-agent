package org.ressourcesarea.MSAgent.integrationTestEndPoints;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class GradeIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getGrade() throws Exception {
        mockMvc.perform(get("/grade/getAllGrade")
        .header("origin","origin"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(9)));
    }
}
